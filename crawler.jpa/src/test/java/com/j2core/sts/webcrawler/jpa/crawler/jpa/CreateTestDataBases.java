package com.j2core.sts.webcrawler.jpa.crawler.jpa;

import com.j2core.sts.webcrawler.jpa.dao.jpa.hibernatedao.dto.UrlData;
import com.j2core.sts.webcrawler.jpa.jsoupinteraction.jpa.dto.URLStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by sts on 11/14/16.
 */
public class CreateTestDataBases {

    public static void main(String[] args) {

        List<UrlData> firstData = new LinkedList<>();

        firstData.add(new UrlData("https://en.wikipedia.org/wiki/Spring_Framework", 1, URLStatus.NOT_PROCESSED));
        firstData.add(new UrlData("http://j2w.blogspot.com", 1, URLStatus.NOT_PROCESSED));
        firstData.add(new UrlData("http://docs.oracle.com/javase/7/docs/technotes/tools/windows/classpath.html", 1, URLStatus.NOT_PROCESSED));
        firstData.add(new UrlData("https://en.wikipedia.org/wiki/Universally_unique_identifier", 1, URLStatus.NOT_PROCESSED));
        firstData.add(new UrlData("http://community.actian.com/wiki/SQL_BOOLEAN_type", 1, URLStatus.NOT_PROCESSED));
        firstData.add(new UrlData("https://www.blueapron.com", 1, URLStatus.NOT_PROCESSED));
        firstData.add(new UrlData("https://www.wellsfargo.com", 1, URLStatus.NOT_PROCESSED));
        firstData.add(new UrlData("http://www.javaworld.com", 1, URLStatus.NOT_PROCESSED));
        firstData.add(new UrlData("https://docs.oracle.com", 1, URLStatus.NOT_PROCESSED));
        firstData.add(new UrlData("http://www.zpub.com/sf/history/", 1, URLStatus.NOT_PROCESSED));
        firstData.add(new UrlData("http://www.goodreads.com/genres/history", 1, URLStatus.NOT_PROCESSED));

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("webCrawler");

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();

        for (UrlData data : firstData) {

            entityManager.persist(data);

        }

        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();

    }
}
