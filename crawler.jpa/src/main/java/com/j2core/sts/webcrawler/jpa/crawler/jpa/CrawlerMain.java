package com.j2core.sts.webcrawler.jpa.crawler.jpa;

import com.j2core.sts.webcrawler.jpa.dao.jpa.WorkerDB;
import com.j2core.sts.webcrawler.jpa.dao.jpa.hibernatedao.HibernateWorkerDB;
import org.apache.log4j.Logger;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * The main class for start web crawler
 */
public class CrawlerMain {


    private final static Logger LOGGER = Logger.getLogger(CrawlerMain.class);         // class for save logs information
    private final static int amountMilliSecondDeprecate = 60000;

    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("webCrawler");
        WorkerDB workerDB1 = new HibernateWorkerDB(entityManagerFactory, amountMilliSecondDeprecate);
        Coordinator coordinator = new Coordinator(10, 5, "Coordinator", workerDB1);
        Thread threadCoordinator = new Thread(coordinator);

        threadCoordinator.start();

        try {
            threadCoordinator.join();
        } catch (InterruptedException e) {
            LOGGER.error(" Sorry");
        }

        entityManagerFactory.close();

        LOGGER.info("stop main");

    }
}
