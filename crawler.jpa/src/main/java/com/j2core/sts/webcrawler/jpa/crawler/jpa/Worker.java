package com.j2core.sts.webcrawler.jpa.crawler.jpa;

import com.j2core.sts.webcrawler.jpa.jsoupinteraction.jpa.PagesAnalyser;
import com.j2core.sts.webcrawler.jpa.jsoupinteraction.jpa.dto.ResultingInformation;
import com.j2core.sts.webcrawler.jpa.jsoupinteraction.jpa.dto.UrlsInformation;
import org.apache.log4j.Logger;

import java.util.concurrent.BlockingQueue;

/**
 * Created by sts on 8/2/16.
 */

/**
 * Class analysed page's information, content, new link, and index all words from page
 */
public class Worker implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(Worker.class);  // object for save log information
    private BlockingQueue<UrlsInformation> pagesLink;
    private BlockingQueue<ResultingInformation> analysedPages;
    private BlockingQueue<UrlsInformation> processesPages;
    private PagesAnalyser pagesAnalyser;
    private final Object sync;

    /**
     * Constructor Worker's class
     * @param pagesLink           collection with page's links (URLs)
     * @param processesLink       collection with URL's which in processes
     * @param analysedPages       collection for saved analysed page's information
     * @param pagesAnalyser       object for analysed page
     * @param sync                object for synchronized
     */
    public Worker(BlockingQueue<UrlsInformation> pagesLink, BlockingQueue<UrlsInformation> processesLink,
                  BlockingQueue<ResultingInformation> analysedPages, PagesAnalyser pagesAnalyser, Object sync) {
        this.pagesLink = pagesLink;
        this.processesPages = processesLink;
        this.analysedPages = analysedPages;
        this.pagesAnalyser = pagesAnalyser;
        this.sync = sync;
    }

    @Override
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = {"WA_NOT_IN_LOOP", "UW_UNCOND_WAIT"}, justification = "Exception detail hide")
    public void run() {

        ResultingInformation newInformation;
        UrlsInformation urlsInformation;

        if (pagesLink.isEmpty()) {
            synchronized (sync) {
                sync.notifyAll();
            }
            synchronized (sync){
                try {
                    sync.wait();
                } catch (InterruptedException e) {
                    LOGGER.error(e);
                }
            }
        } else {
            try {
                urlsInformation = pagesLink.take();
                processesPages.add(urlsInformation);

                if (urlsInformation.getAmountTransition() < pagesAnalyser.getMaxAmountTransition()) {
                    newInformation = pagesAnalyser.analysePageInformation(urlsInformation);

                    if (newInformation != null) {

                        boolean added = analysedPages.add(newInformation);
                        while (!added) {
                            added = analysedPages.add(newInformation);
                        }
                        processesPages.remove(urlsInformation);
                    }else {

                        if (urlsInformation.getAmountReadPage() > -1) {
                            processesPages.remove(urlsInformation);
                            pagesLink.add(urlsInformation);
                        }
                    }
                }
            } catch (InterruptedException e) {
                LOGGER.error(e);
            }
        }
    }
}
