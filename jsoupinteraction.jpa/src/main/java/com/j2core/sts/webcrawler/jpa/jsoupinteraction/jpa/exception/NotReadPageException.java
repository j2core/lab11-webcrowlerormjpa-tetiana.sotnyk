package com.j2core.sts.webcrawler.jpa.jsoupinteraction.jpa.exception;

/**
 * Created by sts on 11/14/16.
 */
public class NotReadPageException extends Exception{


    public NotReadPageException() {
    }

    public NotReadPageException(String message) {
        super(message);
    }
}
