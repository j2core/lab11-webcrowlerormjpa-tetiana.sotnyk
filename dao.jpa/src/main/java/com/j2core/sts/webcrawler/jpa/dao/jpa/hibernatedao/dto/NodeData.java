package com.j2core.sts.webcrawler.jpa.dao.jpa.hibernatedao.dto;

import javax.persistence.*;

/**
 * Created by sts on 12/1/16.
 */
@Entity
@Table(name = "nodeData")
public class NodeData {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "nodeId")
    private int nodeId;

    @Column(name = "nodeName", length = 65535)
    private String nodeName;

    @Column(name = "statusWork")
    private boolean statusWork = true;

    @Column(name = "startUnixTime", length = 65535)
    private long startUnixTime;

    @Column(name = "stopUnixTime", length = 65535)
    private long stopUnixTime;

    @Column(name = "stoppedFlag")
    private boolean stoppedFlag = false;

    public NodeData(){

    }

    public NodeData (String name){
        this.nodeName = name;
        this.statusWork = true;
        this.startUnixTime = System.currentTimeMillis();
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public boolean isStatusWork() {
        return statusWork;
    }

    public void setStatusWork(boolean statusWork) {
        this.statusWork = statusWork;
    }

    public long getStartTime() {
        return startUnixTime;
    }

    public void setStartTime(long startUnixTime) {
        this.startUnixTime = startUnixTime;
    }

    public long getStopTime() {
        return stopUnixTime;
    }

    public void setStopTime(long stopUnixTime) {
        this.stopUnixTime = stopUnixTime;
    }

    public boolean isStoppedFlag() {
        return stoppedFlag;
    }

    public void setStoppedFlag(boolean stoppedFlag) {
        this.stoppedFlag = stoppedFlag;
    }
}
