package com.j2core.sts.webcrawler.jpa.dao.jpa.hibernatedao;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.j2core.sts.webcrawler.jpa.dao.jpa.WorkerDB;
import com.j2core.sts.webcrawler.jpa.dao.jpa.exception.DBException;
import com.j2core.sts.webcrawler.jpa.dao.jpa.hibernatedao.dto.NodeData;
import com.j2core.sts.webcrawler.jpa.dao.jpa.hibernatedao.dto.PageInformation;
import com.j2core.sts.webcrawler.jpa.dao.jpa.hibernatedao.dto.UrlData;
import com.j2core.sts.webcrawler.jpa.dao.jpa.hibernatedao.dto.WordInformation;
import com.j2core.sts.webcrawler.jpa.jsoupinteraction.jpa.dto.ResultingInformation;
import com.j2core.sts.webcrawler.jpa.jsoupinteraction.jpa.dto.URLStatus;
import com.j2core.sts.webcrawler.jpa.jsoupinteraction.jpa.dto.UrlsInformation;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import java.util.*;
import java.util.concurrent.BlockingQueue;

/**
 * Class for work with DB and use JPA annotation
 */
public class HibernateWorkerDB implements WorkerDB {


    private final static Logger LOGGER = Logger.getLogger(HibernateWorkerDB.class); // class for save logs information
    private EntityManagerFactory entityManagerFactory;
    private int amountMillisecondDeprecate;

    /**
     * Constructor
     *
     * @param entityManagerFactory      entity manager factory
     */
    public HibernateWorkerDB(EntityManagerFactory entityManagerFactory, int amountMillisecondDeprecate){

        this.entityManagerFactory = entityManagerFactory;
        this.amountMillisecondDeprecate = amountMillisecondDeprecate;

    }


    @Override
    public List<UrlsInformation> getUrlInformation(int amountLink, int nodeId) {

        List<UrlData> dataList;
        List<UrlsInformation> result = new LinkedList<>();

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        try {

            dataList = entityManager.createQuery("select e from UrlData e where e.status = :status").
                    setParameter("status", URLStatus.NOT_PROCESSED).setMaxResults(amountLink).getResultList();

            if (changeURLStatus(URLStatus.PROCESSES, dataList) && changeNodeId(dataList, nodeId)) {

                for (UrlData data : dataList) {

                    UrlsInformation information = new UrlsInformation(data.getUrlId(), data.getUrl(), data.getAmountTransition(), nodeId);
                    information.setStatus(URLStatus.PROCESSES);
                    result.add(information);

                }

            }
        }catch (Exception ex){
            LOGGER.error(ex);
        }finally {
            entityManager.close();
        }

        return result;
    }


    @Override
    public void finalSaveInformation(BlockingQueue<ResultingInformation> analysedPages, BlockingQueue<UrlsInformation> processesLink, BlockingQueue<UrlsInformation> pagesLink, int nodeId) {

        if (!analysedPages.isEmpty()){

            for (ResultingInformation resultingInformation : analysedPages) {
                try {
                    if (!addInformation(resultingInformation)){
                        processesLink.add(new UrlsInformation(resultingInformation.getUrlId(), resultingInformation.getPageUrl(),
                                resultingInformation.getAmountTransition(), resultingInformation.getNodeId()));
                    }
                } catch (DBException e) {
                    e.printStackTrace();
                }
            }

        }

        if (!processesLink.isEmpty()) {
            pagesLink.addAll(processesLink);
        }
        if (!pagesLink.isEmpty()) {

            List<UrlData> lastData = changeCollection(pagesLink);
            lastData = equalsNodeId(lastData, nodeId);
            changeURLStatus(URLStatus.NOT_PROCESSED, lastData);
            changeNodeId(lastData, null);

        }

    }


    /**
     * The method do equal with work node's id and node's id witch wrote in URL's data
     *
     * @param lastData   URL's data witch need equals
     * @param id         work node's id
     * @return  collection UrlData with work node's id
     */
    private List<UrlData> equalsNodeId(List<UrlData> lastData, int id) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Integer nodeId = id;

        try {

            Iterator<UrlData> urlDataIterator = lastData.iterator();
            while (urlDataIterator.hasNext()) {
                UrlData data = urlDataIterator.next();
                UrlData dbData = entityManager.find(UrlData.class, data.getUrlId());
                if (!nodeId.equals(dbData.getNodeId())) {
                    urlDataIterator.remove();
                }
            }
        } finally {
            entityManager.close();
        }
        return lastData;
    }


    @Override
    public boolean addInformation(ResultingInformation resultingInformation) throws DBException {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        boolean flagAddPage = false;

        try{

            UrlData urlData = entityManager.find(UrlData.class, resultingInformation.getUrlId());

            if (urlData.getNodeId() == resultingInformation.getNodeId()) {

                entityManager.getTransaction().begin();

                PageInformation pageInformation = new PageInformation(resultingInformation.getPagesText(), new Date(), urlData);

                try {
                    entityManager.createQuery("select e from PageInformation e where e.urlData.urlId = ?1").
                            setParameter(1, urlData.getUrlId()).getSingleResult();
                } catch (NoResultException ex) {

                    while (!flagAddPage) {
                        flagAddPage = true;
                        try {
                            entityManager.persist(pageInformation);
                        } catch (Exception e) {
                            flagAddPage = false;
                        }
                    }
                }

                addWordInformation(entityManager, urlData, pageInformation, resultingInformation.getWordsInPage());

                addNewUrlInformation(entityManager, resultingInformation.getUrlCollectionNew());

                changeURLStatus(entityManager, URLStatus.PROCESSED, resultingInformation.getUrlId());

                entityManager.getTransaction().commit();

            }else throw new Exception(" This URL do not belong this node");

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;

        }finally {
            entityManager.close();
        }

        return true;

    }


    /**
     * The method change node's id in URLData
     *
     * @param dataList       collection with URLData
     * @param nodeId         node's Id
     * @return   change node's Id successfully or no
     */
    private boolean changeNodeId(List<UrlData> dataList, Integer nodeId){

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Multiset<Integer> nodeIdWithDeprecateData = HashMultiset.create();

        try {

            entityManager.getTransaction().begin();

            for (UrlData data : dataList) {

                data.setNodeId(nodeId);
                entityManager.merge(data);
                nodeIdWithDeprecateData.add(data.getNodeId());

            }

            entityManager.getTransaction().commit();

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;
        }finally {
            entityManager.close();
        }
        return true;
    }


    /**
     * The method change URL's status
     *
     * @param urlStatus         URL's status
     * @param dataList          collection with URL data
     * @return change URL's status successfully or no
     */
    public boolean changeURLStatus(URLStatus urlStatus, List<UrlData> dataList) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        try {

            entityManager.getTransaction().begin();

            for (UrlData data : dataList) {

                data.setStatusChangeTime(System.currentTimeMillis());
                data.setStatus(urlStatus);
                entityManager.merge(data);

            }

            entityManager.getTransaction().commit();

        }catch (Exception ex){
            LOGGER.error(ex);
            return false;
        }finally {
            entityManager.close();
        }

        return true;

    }


    /**
     * The method change URL's status
     *
     * @param entityManager     entity manager
     * @param urlStatus         URL's status
     * @param urlId             URL's id
     * @return change URL's status successfully or no
     */
    private boolean changeURLStatus(EntityManager entityManager, URLStatus urlStatus, int urlId) {

        try {

            UrlData data = entityManager.find(UrlData.class, urlId);

            data.setStatus(urlStatus);
            data.setStatusChangeTime(System.currentTimeMillis());
            entityManager.merge(data);

        } catch (Exception ex) {
            LOGGER.error(ex);
            return false;
        }

        return true;

    }


    /**
     * The method change collection with UrlInformation to collection with UrlData
     *
     * @param pagesLink      collection with UrlInformation
     * @return  collection with UrlData
     */
    private List<UrlData> changeCollection(BlockingQueue<UrlsInformation> pagesLink){

        List<UrlData> result = new LinkedList<>();

        for (UrlsInformation urlsInformation : pagesLink){

            UrlData urlData = new UrlData(urlsInformation.getPagesUrl(), urlsInformation.getAmountTransition(), urlsInformation.getStatus());
            urlData.setUrlId(urlsInformation.getId());

            result.add(urlData);

        }

        return result;
    }


    /**
     * The method add word information from page to the DB
     *
     * @param entityManager          entity manager
     * @param urlData                URL's data
     * @param pageData               page's data
     * @param wordsInPage            collection with words from page
     * @return add word in DB successfully or no
     */
    public boolean addWordInformation(EntityManager entityManager, UrlData urlData, PageInformation pageData, Multiset<String> wordsInPage){

        try{
            Set<Multiset.Entry<String>> wordsCollection = wordsInPage.entrySet();

            for (Multiset.Entry<String> word : wordsCollection){

                WordInformation wordData = new WordInformation(urlData, pageData, word.getElement(), word.getCount());

                entityManager.persist(wordData);

            }

        }catch (Exception e){

            LOGGER.error(e);
            return false;
        }

        return true;
    }


    /**
     * The method add new URL in the DB
     *
     * @param entityManager         entity manager
     * @param newUrlInformation     collection with new URLs
     * @return add new URLs in DB successfully or no
     */
    public boolean addNewUrlInformation(EntityManager entityManager, Set<UrlsInformation> newUrlInformation) {

        UrlData urlData;

        try {

            for (UrlsInformation data : newUrlInformation) {

                urlData = new UrlData(data.getPagesUrl(), data.getAmountTransition(), URLStatus.NOT_PROCESSED);
                try {
                    entityManager.createQuery("select e from UrlData e where e.url = :url").
                            setParameter("url", data.getPagesUrl()).getSingleResult();
                }catch (NoResultException ex){

                    entityManager.persist(urlData);

                }
            }

        }
        catch (Exception e) {

            LOGGER.error(e);
            return false;
        }

        return true;
    }


    @Override
    public NodeData addNode(String nodeName){

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        NodeData nodeData = new NodeData(nodeName);

        try {
            entityManager.getTransaction().begin();

            entityManager.persist(nodeData);

            entityManager.getTransaction().commit();

        }catch (Exception ex){
            LOGGER.error(ex);
            return null;
        }finally {
            entityManager.close();
        }
        return nodeData;
    }


    @Override
    public void stopNode(NodeData nodeData) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        nodeData.setStatusWork(false);
        nodeData.setStopTime(System.currentTimeMillis());

        try {
            entityManager.getTransaction().begin();

            entityManager.merge(nodeData);

            entityManager.getTransaction().commit();

        }catch (Exception ex){
            LOGGER.error(ex);
        }finally {
            entityManager.close();
        }
    }


    /**
     * The method return deprecate data in to work
     * @return  return date successfully or no
     */
    public boolean returnDeprecateData(){

        List<UrlData> dataList;
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        long unixTime = System.currentTimeMillis();
        long oldUnixTime = unixTime - amountMillisecondDeprecate;

        try {

            dataList = entityManager.createQuery("select e from UrlData e where e.status = :status and e.statusChangeUnixTime < :statusChangeUnixTime").
                    setParameter("status", URLStatus.PROCESSES).setParameter("statusChangeUnixTime", oldUnixTime).getResultList();

            if (!dataList.isEmpty()){

                changeURLStatus(URLStatus.NOT_PROCESSED, dataList);
                changeNodeId(dataList, null);
                LOGGER.info(" Amount Deprecate data is = " + dataList.size() + " all data returned for processes.");

            }

        }catch (Exception ex){
            LOGGER.error(ex);
            return false;
        }finally {
            entityManager.close();
        }

        return true;
    }


    /**
     * The method get information about status stoppedFlag in DB
     *
     * @param nodeId  node's Id
     * @return status stoppedFlag in DB : true or false
     */
    public boolean getStoppedFlag(int nodeId){

        NodeData node = null;
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        try {

            node = entityManager.find(NodeData.class, nodeId);

        }catch (Exception ex){
            LOGGER.error(ex);
        }finally {
            entityManager.close();
        }

        assert node != null;
        return node.isStoppedFlag();
    }

}
