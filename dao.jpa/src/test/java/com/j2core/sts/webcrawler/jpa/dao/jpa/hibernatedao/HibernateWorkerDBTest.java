package com.j2core.sts.webcrawler.jpa.dao.jpa.hibernatedao;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.j2core.sts.webcrawler.jpa.dao.jpa.exception.DBException;
import com.j2core.sts.webcrawler.jpa.dao.jpa.hibernatedao.dto.PageInformation;
import com.j2core.sts.webcrawler.jpa.dao.jpa.hibernatedao.dto.UrlData;
import com.j2core.sts.webcrawler.jpa.dao.jpa.hibernatedao.dto.WordInformation;
import com.j2core.sts.webcrawler.jpa.jsoupinteraction.jpa.dto.ResultingInformation;
import com.j2core.sts.webcrawler.jpa.jsoupinteraction.jpa.dto.URLStatus;
import com.j2core.sts.webcrawler.jpa.jsoupinteraction.jpa.dto.UrlsInformation;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.*;

/**
 * Created by sts on 11/4/16.
 */
public class HibernateWorkerDBTest {

    private EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("psunit2");


    @Test(enabled = false)
    public void testGetUrlInformation() {

        int amountUrl = 3;
        HibernateWorkerDB worker = new HibernateWorkerDB(entityManagerFactory, 30);

        List dataList = worker.getUrlInformation(amountUrl, 1);

        Assert.assertTrue(dataList.size() == amountUrl);

    }


    @Test(enabled = false)
    public void testGetUrlInformationChangeStatus() {

        HibernateWorkerDB worker = new HibernateWorkerDB(entityManagerFactory, 30);
        int amountLink = 2;
        List dataList;
        List dataInDB;
        int result = amountLink;

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        dataInDB = entityManager.createQuery("select e from UrlData e where e.status = :status").
                setParameter("status", URLStatus.PROCESSES).getResultList();

        worker.getUrlInformation(amountLink, 1);

        dataList = entityManager.createQuery("select e from UrlData e where e.status = :status").
                setParameter("status", URLStatus.PROCESSES).getResultList();

        if (!dataInDB.isEmpty()) {
            result = result + dataInDB.size();
        }

        Assert.assertTrue(dataList.size() == result);

    }


    @BeforeGroups("addInformation")
    public ResultingInformation createResultInformation(){

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        UrlData urlData = (UrlData) entityManager.createQuery("select e from UrlData e where e.status = :status").
                setParameter("status", URLStatus.NOT_PROCESSED).setMaxResults(1).getResultList().get(0);

        entityManager.close();

        ResultingInformation resultingInformation = new ResultingInformation(urlData.getUrlId(), urlData.getUrl(), urlData.getAmountTransition(), 1);

        Set<UrlsInformation> newUrl = new HashSet<>();
        newUrl.add(new UrlsInformation(-1, "http://stackoverflow.com/post123/link/url/1234", 3, -1));

        Multiset<String> wordsInPage = HashMultiset.create();
        wordsInPage.add("good");

        resultingInformation.setUrlCollectionNew(newUrl);
        resultingInformation.setWordsInPage(wordsInPage);
        resultingInformation.setPagesText("JPA: How to get entity based on field value other than ID?jsbf,bsfr,bsrrkfmhbsmfbsmdhfb");

        return resultingInformation;
    }


    @Test(groups = "addInformation", enabled = false)
    public void testAddInformation() throws DBException {

        HibernateWorkerDB worker = new HibernateWorkerDB(entityManagerFactory, 30);
        ResultingInformation information = createResultInformation();

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        List amountUrl = entityManager.createQuery("from UrlData", UrlData.class).getResultList();
        List amountPage = entityManager.createQuery("from PageInformation", PageInformation.class).getResultList();
        List amountWord = entityManager.createQuery("from WordInformation", WordInformation.class).getResultList();
        List processedUrl = entityManager.createQuery("select e from UrlData e where e.status = :status").
                setParameter("status", URLStatus.PROCESSED).getResultList();

        worker.addInformation(information);

        List resultUrl = entityManager.createQuery("from UrlData", UrlData.class).getResultList();
        List resultPage = entityManager.createQuery("from PageInformation", PageInformation.class).getResultList();
        List resultWord = entityManager.createQuery("from WordInformation", WordInformation.class).getResultList();
        List resultProcessedUrl = entityManager.createQuery("select e from UrlData e where e.status = :status").
                setParameter("status", URLStatus.PROCESSED).getResultList();

        entityManager.close();

        Assert.assertTrue((amountUrl.size() + 1) == resultUrl.size() && (amountPage.size() + 1) == resultPage.size() &&
                (amountWord.size() + 1) == resultWord.size() && (processedUrl.size() + 1) == resultProcessedUrl.size());

    }


    @Test(enabled = false)
    public void testChangeURLStatus() {

        HibernateWorkerDB worker = new HibernateWorkerDB(entityManagerFactory, 30);
        List dataList;
        List dataAfter;
        List dataInDB;
        int amountLink = 2;
        int result = amountLink;

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        dataInDB = entityManager.createQuery("select e from UrlData e where e.status = :status").
                setParameter("status", URLStatus.PROCESSED).getResultList();

        dataList = entityManager.createQuery("select e from UrlData e where e.status = :status").
                setParameter("status", URLStatus.NOT_PROCESSED).setMaxResults(amountLink).getResultList();

        worker.changeURLStatus(URLStatus.PROCESSED, dataList);

        dataAfter = entityManager.createQuery("select e from UrlData e where e.status = :status").
                setParameter("status", URLStatus.PROCESSED).getResultList();

        if (!dataInDB.isEmpty()){
            result = result + dataInDB.size();
        }

        Assert.assertTrue(dataAfter.size() == result);
    }


    @Test(enabled = false)
    public void testAddNewUrlInformation(){

        Set<UrlsInformation> newUrl = new HashSet<>();
        newUrl.add(new UrlsInformation(-1, "http://svnbook.red-bean.com/en/1.7", 3, -1));
        newUrl.add(new UrlsInformation(-1, "http://svnbook.red-bean.com/users", 2, -1));
        newUrl.add(new UrlsInformation(-1, "http://docs.oracle.com/javaee/6/tutorial/doc/bnbrg.html", 2, -1));
        List dataInDB = null;
        List dataList = null;
        int result = newUrl.size() - 1;
        HibernateWorkerDB worker = new HibernateWorkerDB(entityManagerFactory, 30);
        EntityManager entityManager = null;

        try {
            entityManager = entityManagerFactory.createEntityManager();

            dataInDB = entityManager.createQuery("from UrlData", UrlData.class).getResultList();

            entityManager.getTransaction().begin();

            worker.addNewUrlInformation(entityManager, newUrl);

            entityManager.getTransaction().commit();

        } finally {
            assert entityManager != null;
            entityManager.close();
        }

        try {
            entityManager = entityManagerFactory.createEntityManager();

            dataList = entityManager.createQuery("from UrlData", UrlData.class).getResultList();

        } finally {
            entityManager.close();
        }
        if (!dataInDB.isEmpty()){
            result = result + dataInDB.size();
        }

        Assert.assertTrue(dataList.size() == result);
    }


    @Test(enabled = false)
    public void testAddWordInformation(){

        List dataList = null;
        Multiset<String> wordsInPage = HashMultiset.create();
        wordsInPage.add("Hibernate");
        List dataInDB = null;
        int result = wordsInPage.size();
        HibernateWorkerDB worker = new HibernateWorkerDB(entityManagerFactory, 30);
        EntityManager  entityManager = entityManagerFactory.createEntityManager();

        try {

            dataInDB  = entityManager.createQuery("from WordInformation", WordInformation.class).getResultList();

            entityManager.getTransaction().begin();

            UrlData urlData = entityManager.find(UrlData.class, 10);
            PageInformation pageInformation = new PageInformation("mhrbgmrbgmbgmebg,mbergmbdegmbegmnbaer,mgb,amnerg  hjkhfgskfghkskgfj", new Date(), urlData);
            entityManager.persist(pageInformation);

            worker.addWordInformation(entityManager, urlData, pageInformation, wordsInPage);

            entityManager.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            dataList  = entityManager.createQuery("from WordInformation", WordInformation.class).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!dataInDB.isEmpty()){
            result = result + dataInDB.size();
        }

        entityManager.close();

        Assert.assertTrue(dataList.size() == result);

    }

    @AfterClass
    public void closeEntityManager(){

        entityManagerFactory.close();

    }
}
